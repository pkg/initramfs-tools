Source: initramfs-tools
Section: utils
Priority: optional
Uploaders: Michael Prokop <mika@debian.org>, Ben Hutchings <benh@debian.org>
Maintainer: Debian kernel team <debian-kernel@lists.debian.org>
Build-Depends: bash-completion, debhelper-compat (= 13), shunit2 <!nocheck>
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/kernel-team/initramfs-tools
Vcs-Git: https://salsa.debian.org/kernel-team/initramfs-tools.git

Package: initramfs-tools
Architecture: all
Multi-Arch: foreign
Depends: initramfs-tools-core (= ${binary:Version}),
         linux-base,
         ${misc:Depends}
Suggests: bash-completion
Provides: linux-initramfs-tool
Conflicts: linux-initramfs-tool
Description: generic modular initramfs generator (automation)
 This package builds a bootable initramfs for Linux kernel packages.  The
 initramfs is loaded along with the kernel and is responsible for
 mounting the root filesystem and starting the main init system.

Package: initramfs-tools-core
Architecture: all
Multi-Arch: foreign
Recommends: zstd, ${busybox:Recommends}
Depends: coreutils (>= 8.24),
         cpio (>= 2.12),
         dracut-install,
         klibc-utils (>= 2.0.4-8~),
         kmod,
         logsave | e2fsprogs (<< 1.45.3-1~),
         udev,
         ${misc:Depends}
Suggests: bash-completion
Description: generic modular initramfs generator (core tools)
 This package contains the mkinitramfs program that can be used to
 create a bootable initramfs for a Linux kernel.  The initramfs should
 be loaded along with the kernel and is then responsible for mounting
 the root filesystem and starting the main init system.
